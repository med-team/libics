libics (1.6.8-1) unstable; urgency=medium

  * New upstream version 1.6.8
  * d/copyright: update copyright year and upstream change.
  * d/control: add myself to uploaders.
  * d/control: declare compliance to standards version 4.7.0.
  * d/libics0.symbols: declare Build-Depends-Package field.
  * ignore_failing_test.patch: delete: issue resolved upstream.

 -- Étienne Mollier <emollier@debian.org>  Sun, 03 Nov 2024 17:51:03 +0100

libics (1.6.6-1) unstable; urgency=medium

  * Fix watch file
  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 29 Nov 2022 11:27:06 +0100

libics (1.6.5-2) unstable; urgency=medium

  * d/watch: Add filenamemangle
  * Do not build anything when just creating doc package
    Closes: #996334

 -- Andreas Tille <tille@debian.org>  Wed, 13 Oct 2021 15:44:31 +0200

libics (1.6.5-1) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Remove Section on libics0 that duplicates source.
  * Apply multi-arch hints.
    + libics-dev, libics0: Add Multi-Arch: same.
  * Prevent lintian-brush from removing duplicate section entry which is
    needed by d-shlibs by using Section: science for source package
  * Deal with unneded *.la file

 -- Andreas Tille <tille@debian.org>  Wed, 13 Oct 2021 09:53:43 +0200

libics (1.6.4-1) unstable; urgency=medium

  [ Michael R. Crusoe ]
  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add symbols file
  * Run the tests serially
    Closes: #953191

  [ Andreas Tille ]
  * Add salsa-ci file (routine-update)

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Sat, 07 Mar 2020 08:10:12 +0100

libics (1.6.3-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Ignore failing test

 -- Andreas Tille <tille@debian.org>  Thu, 01 Aug 2019 14:07:35 +0200

libics (1.6.2-2) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.2.1

 -- Andreas Tille <tille@debian.org>  Thu, 01 Nov 2018 15:18:22 +0100

libics (1.6.2-1) unstable; urgency=medium

  * New upstream version
    Closes: #882430

 -- Andreas Tille <tille@debian.org>  Sat, 02 Dec 2017 08:15:35 +0100

libics (1.6.1-1) unstable; urgency=medium

  [ Mathieu Malaterre ]
  * Remove self from Uploaders

  [ Andreas Tille ]
  * New upstream version
  * Moved packaging from SVN to Git
  * debhelper 10
  * Standards-Version: 4.1.1
  * Project moved to Github
  * hardening=+all
  * I'm to lazy to maintain symbols files for libs without rdepends
  * Use d-shlibs
  * DEP5
  * Distinct short descriptions for packages

 -- Andreas Tille <tille@debian.org>  Wed, 22 Nov 2017 12:55:06 +0100

libics (1.5.2-6) unstable; urgency=medium

  * Fix arch only builds
    Closes: #750617

 -- Andreas Tille <tille@debian.org>  Thu, 05 Jun 2014 09:32:22 +0200

libics (1.5.2-5) unstable; urgency=medium

  * use dh-autoreconf instead of autotools-dev to fix FTBFS on ppc64el
    (thanks for the patch to Logan Rosen <logan@ubuntu.com>)
    Closes: #735636
  * cme fix dpkg-control
  * debian/rules: remove privacy-breach-logo

 -- Andreas Tille <tille@debian.org>  Tue, 13 May 2014 15:25:22 +0200

libics (1.5.2-4) unstable; urgency=low

  * Add symbols file
  * Bump to Std-Vers 3.9.4.
    - d/rules: move install target to install-arch

 -- Mathieu Malaterre <malat@debian.org>  Sun, 09 Jun 2013 17:01:57 +0200

libics (1.5.2-3) unstable; urgency=low

  * Use dh(9), get hardening for free
  * Add libics-doc package for documentation
  * Make multiarch

 -- Mathieu Malaterre <malat@debian.org>  Wed, 11 Apr 2012 18:57:00 +0200

libics (1.5.2-2) unstable; urgency=low

  * Pull svn rev 10 to fix big endian issues.

 -- Mathieu Malaterre <malat@debian.org>  Thu, 05 Apr 2012 09:05:59 +0200

libics (1.5.2-1) unstable; urgency=low

  * Initial Debian Upload (Closes: #666082)

 -- Mathieu Malaterre <malat@debian.org>  Wed, 28 Mar 2012 17:28:11 +0200
