Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: libics
Source: https://github.com/svi-opensource/libics

Files: *
Copyright: © 2000-2013 Cris Luengo and others
           © 2015-2024 Scientific Volume Imaging Holding B.V.
License: LGPL-2+

Files: libics_gzip.c
Copyright: 1995-1998 Jean-loup Gailly and Mark Adler
License: BSD-3-clause
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

Files: debian/*
Copyright: © 2012 Mathieu Malaterre <malat@debian.org>
License: LGPL-2+

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 You should have received a copy of the GNU Library General Public
 License along with this library; if not, write to the Free
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU Library General Public License
 can be found in /usr/share/common-licenses/LGPL-2 file.

